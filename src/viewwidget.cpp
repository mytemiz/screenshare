#include "viewwidget.h"
#include <QPainter>
#include <QPainterPath>
#include <QImage>
#include <QDebug>
#include <QGuiApplication>
#include <QScreen>
#include <QWindow>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QCursor>
#include "tcursor.h"

ViewWidget::ViewWidget(QWidget *parent) : QWidget(parent)
{
    setMinimumSize(500,500);
    setFocusPolicy(Qt::ClickFocus);
}

void ViewWidget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.setClipping(true);
    QPainterPath mpath;
    QImage image("/home/pcmos/Pictures/vektoral/starwars.jpg");
    image = image.convertToFormat(QImage::Format_Mono);

    QScreen *screen = QGuiApplication::primaryScreen();
    if (const QWindow *window = windowHandle())
        screen = window->screen();
    if (!screen)
        return;

    QPixmap scpix = screen->grabWindow(0);
    QRectF origRect = scpix.rect();
    scpix = scpix.scaled(rect().width(), rect().height(), Qt::KeepAspectRatio);
    QRectF scaledRect = scpix.rect();
    _scale.setX(origRect.width()/scaledRect.width());
    _scale.setY(origRect.height()/scaledRect.height());

    QPointF center(image.width()/2.0, image.height()/2.0);
    mpath.addEllipse(center, 150,200);
//    p.setClipPath(mpath);
    p.drawPixmap(QPointF(0,0),scpix);
//    p.drawImage(QPointF(0,0),image);
}

void ViewWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_A:
    {
        QPoint pos = QCursor::pos();
        QPoint mapped = mapFromGlobal(pos);
        QPointF scaled;
        scaled.setX(mapped.x()*_scale.x());
        scaled.setY(mapped.y()*_scale.y());
        qDebug() << pos << mapped << scaled <<_scale;
        QCursor::setPos(scaled.toPoint());
        TCursor cur;
        cur.click(Button1);
    }
        break;
    default:
        break;
    }
}

void ViewWidget::mousePressEvent(QMouseEvent *event)
{
    switch (event->button()) {
    case Qt::LeftButton:
    {
        QPoint pos = QCursor::pos();
        QPoint mapped = mapFromGlobal(pos);
        QPointF scaled;
        scaled.setX(mapped.x()*_scale.x());
        scaled.setY(mapped.y()*_scale.y());
        qDebug() << pos << mapped << scaled <<_scale;
        QCursor::setPos(scaled.toPoint());
        TCursor cur;
        cur.click(Button1);
    }
        break;
    default:
        break;
    }
}

void ViewWidget::mouseReleaseEvent(QMouseEvent *event)
{

}
