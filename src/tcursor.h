#ifndef TCURSOR_H
#define TCURSOR_H

#include <X11/Xlib.h>
#include <stdio.h>
#include <string.h>

class TCursor
{
public:
    TCursor();

    ~TCursor();

    void click (int button);

    void coords (int *x, int *y);

    void move (int x, int y);

    void move_to (int x, int y);

    void pixel_color (int x, int y, XColor *color);

private:
    Display *display = NULL;

};

#endif // TCURSOR_H
