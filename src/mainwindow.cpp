#include "mainwindow.h"
#include <QImage>
#include <QPixmap>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    _vw = new ViewWidget();

    setCentralWidget(_vw);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateView()));
    timer->start(1000);
}

void MainWindow::updateView()
{
    _vw->update();
}

MainWindow::~MainWindow()
{

}
